﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public AudioSource gameOverSource;
	public AudioSource termEffects;
	public AudioSource bonusEffects;

	public static SoundManager instance = null;

	public void PlayTermClip(AudioClip clip)
	{
		termEffects.clip = clip;
		termEffects.Play ();
	}

	public void StopTermClip()
	{
		termEffects.Stop ();
	}

	public void PlayBonusClip(AudioClip clip)
	{
		bonusEffects.clip = clip;
		bonusEffects.Play ();
	}

	public void StopBonusClip()
	{
		bonusEffects.Stop ();
	}

	public void PlayGameOverClip(AudioClip clip)
	{
		gameOverSource.clip = clip;
		gameOverSource.Play ();
	}

	void Awake()
	{
		if (instance == null)
			instance = this;
		else
			Destroy (gameObject);
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
