﻿using UnityEngine;
using System.Collections;
using System;

public class GenerateBonus : MonoBehaviour {

	public float timeToAdd = 30f;
	public float timeToRemove = 20f;
	public GameObject[] bonusArray;
	private float curTime = 0f;
	private System.Random rand;
	private Renderer m_renderer;
	private GameObject bonus;

	// Use this for initialization
	void Start () {
		rand = new System.Random ((int)DateTime.Now.Ticks & 0x0000FFFF);
		m_renderer = GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		curTime += Time.deltaTime;
		if (curTime >= timeToAdd) {
			curTime = 0f;
			GameObject obj = Generate ();
			bonus = Instantiate (obj) as GameObject;
		}
		if (curTime >= timeToRemove && bonus != null)
			Destroy (bonus);
	}

	GameObject Generate()
	{
		int pos = rand.Next (bonusArray.Length);
		GameObject bonus = bonusArray [pos];
		Vector3 size = m_renderer.bounds.size;
		int posX = rand.Next ((int) size.x);
		int posY = rand.Next ((int) size.y);
		Vector3 position = new Vector3 (transform.position.x + posX - size.x * 0.5f, 
			transform.position.y + posY - size.y * 0.5f, 0);
		bonus.transform.position = position;
		return bonus;
	}
}
