﻿using UnityEngine;
using System.Collections;

public class CoinAction : MonoBehaviour {

	public int points = 300;
	public AudioClip coinClip;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.CompareTag ("Ingredient")) {
			GameManager.instance.AddPoints (points, true);
			SoundManager.instance.PlayBonusClip (coinClip);
			Destroy (gameObject);
		}
	}

}
