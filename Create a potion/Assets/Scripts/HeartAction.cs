﻿using UnityEngine;
using System.Collections;

public class HeartAction : MonoBehaviour {

	public AudioClip heartClip;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.CompareTag ("Ingredient")) {
			GameManager.instance.GetComponent<HeartManager> ().addHeart ();
			SoundManager.instance.PlayBonusClip (heartClip);
			Destroy (gameObject);
		}
	}

}
