﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SectorController : MonoBehaviour {

	public Image[] sectors;
	public float sectorLen = 40f;
	public float activeSectorAlpha = .5f;
	public float multInCenter = 2f;

	private int currentSector = 0;
	private Slider termometer;

	public void SetSector(int secNum)
	{
		secNum = Mathf.Max (0, secNum);
		secNum = Mathf.Min (secNum, sectors.Length - 1);
		Color curCol = sectors [currentSector].color;
		sectors [currentSector].color = new Color (curCol.r, curCol.g, curCol.b, 0);
		curCol = sectors [currentSector = secNum].color;
		sectors [currentSector].color = new Color (curCol.r, curCol.g, curCol.b, activeSectorAlpha);
	}

	public float GetMult()
	{
		SectorRange sr = sectors [currentSector].GetComponent<SectorRange> ();
		float median = (sr.left + sr.right) / 2.0f;
		float module = Mathf.Abs(median - termometer.value);
		return multInCenter * Mathf.Max(1f - module / (sr.right - sr.left), 0f);
	}
	// Use this for initialization
	void Awake () 
	{
		termometer = GetComponent<Slider>();		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
