﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TempChanger : MonoBehaviour {

	public Slider temperature;
	public float deltaTemp = 5f;
	public AudioClip tempClip;

	private bool isPointerDown = false;

	public void SetPointerDown()
	{
		isPointerDown = true;
		SoundManager.instance.PlayTermClip (tempClip);
	}

	public void SetPointerUp()
	{
		isPointerDown = false;
		SoundManager.instance.StopTermClip ();
	}
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () {
		if (isPointerDown) 
		{
			temperature.value += deltaTemp * Time.deltaTime;
		}
	}
}
