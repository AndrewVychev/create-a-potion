﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class GameManager : MonoBehaviour {

	public static GameManager instance = null;
	public GameObject[] ingredientsArray;
	public Transform spawn;
	public Transform par;
	public Text scoreText;
	public Slider temperature;
	public Image[] pipeImages;
	public SectorController sectorController;
	public Text finalScoreText;
	public Animator gameOverAnimator;

	public float deltaCauldronSpeed = 0.5f;
	//public float deltaRopeAngle = 0.1f;
	public MovingCauldron movCauldSc;
	public RopeScript ropeScript;
	public Rigidbody2D ropeBody;
	public float deltaVelocity = 50f;
	public float deltaGravity = 0.4f;

	public float timeToChangeSector = 5f;

	private float scorePoints = 0f;
	private int curPipePos = 0;
	private GameObject[] pipeline;
	private System.Random rand;
	private bool isGameOver = false;

	private float schTimer = 15f;

	void Awake()
	{
		if (instance == null)
			instance = this;
		else if (instance != null)
			Destroy (gameObject);
	}

	public void Spawn()
	{
		if (isGameOver)
			return;
		if (curPipePos >= pipeline.Length)
			return;
		GameObject nObjToSpawn = Instantiate (pipeline[0], spawn.position, spawn.rotation) as GameObject;
		nObjToSpawn.GetComponent<Transform> ().parent = par;

		for (int i = 0; i < pipeImages.Length - 1; i++)
				pipeImages [i].sprite = pipeImages [i + 1].sprite;
		for (int i = 0; i < pipeline.Length - 1; i++)
				pipeline [i] = pipeline [i + 1];
		pipeline [pipeline.Length - 1] = GetRandomIngr ();
		pipeImages [pipeImages.Length - 1].sprite = pipeline[pipeImages.Length - 1].GetComponent<SpriteRenderer> ().sprite;
	}

	public void AddPoints(float points, bool good)
	{
		float multipler = 1f;
		multipler *= sectorController.GetMult ();

		points *= multipler;
		float oldPoints = scorePoints;
		if (good)
			scorePoints += points;
		else
			scorePoints -= points;
		scorePoints = Mathf.Max (0f, scorePoints);
		int deltaPoints = ((int) (scorePoints)) / 1000 - ((int)(oldPoints)) / 1000;
		if (deltaPoints > 0) 
		{
			for (int i = 0; i < deltaPoints; i++) 
			{
				movCauldSc.IncreaseSpeed (deltaCauldronSpeed);
				ropeScript.AddVelocity (deltaVelocity);
				ropeScript.AddGravity (deltaGravity);
				//ropeScript.AddAngle (deltaRopeAngle);
				GetComponent<HeartManager> ().addHeart ();
			}
		} else 
		{
			for (int i = 0; i < -deltaPoints; i++)
			{
				movCauldSc.DecreaseSpeed (deltaCauldronSpeed);
				ropeScript.DecVelocity (deltaVelocity);
				ropeScript.DecGravity (deltaGravity);
				//ropeScript.DecAngle (deltaRopeAngle);
			}
		}
		scoreText.text = "Score: " + Mathf.RoundToInt(scorePoints).ToString ();
	}
	// Use this for initialization
	void Start () {
		rand = new System.Random ((int) DateTime.Now.Ticks & 0x0000FFFF);
		pipeline = new GameObject[pipeImages.Length];
		for (int i = 0; i < pipeImages.Length; i++) 
		{
			pipeline [i] = GetRandomIngr ();
			pipeImages [i].sprite = pipeline [i].GetComponent<SpriteRenderer> ().sprite;
		}
		AddPoints (0, false);
		Spawn ();
	}

	private GameObject GetRandomIngr()
	{
		int pos = rand.Next(ingredientsArray.Length);
		return ingredientsArray[pos];
	}

	public void AddTemperature(int t_value)
	{
		if (isGameOver)
			return;
		temperature.value += t_value;
	}

	// Update is called once per frame
	void Update () 
	{
		if (isGameOver)
			return;
		schTimer += Time.deltaTime;
		if (schTimer >= timeToChangeSector) 
		{
			schTimer = 0f;
			sectorController.SetSector(rand.Next(sectorController.sectors.Length));
			GetComponent<FireControl> ().FireControlEvent ();
		}
	}

	public void GameOver()
	{
		isGameOver = true;
		//Time.timeScale = 0;
		finalScoreText.text = "Your result: " + Mathf.RoundToInt (scorePoints);
		gameOverAnimator.SetTrigger ("GameOver");
	}
}
