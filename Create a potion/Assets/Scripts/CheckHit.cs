﻿using UnityEngine;
using System.Collections;

public class CheckHit : MonoBehaviour {

	public bool good = true;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.CompareTag ("Ingredient")) {
			GameManager.instance.AddPoints (other.GetComponent<PutScript> ().scorePoints, good);
			Destroy (other.gameObject);
			GameManager.instance.Spawn ();
			if (!good)
				GameManager.instance.GetComponent<HeartManager> ().removeHeart ();
		}
	}


}
