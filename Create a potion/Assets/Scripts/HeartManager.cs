﻿using UnityEngine;
using System.Collections;

public class HeartManager : MonoBehaviour {

	public Transform startPosition;
	public int count = 5;
	public GameObject heart;
	public float padding = 0f;
	private Stack heartArray;
	private RectTransform rt;

	// Use this for initialization
	void Start () {
		heartArray = new Stack ();
		rt = heart.GetComponent<RectTransform> ();
		for (int i = 0; i < count; i++)
			heartArray.Push (instHeart ());
	}

	public GameObject instHeart()
	{
		float width = rt.rect.width;
		Vector3 position;
		if (heartArray.Count == 0)
			position = startPosition.position;
		else
			position = (heartArray.Peek () as GameObject).transform.position;
		position = new Vector3 (position.x + width + padding, position.y, position.z);
		GameObject newObj = Instantiate (heart, position, new Quaternion()) as GameObject;
		return newObj;
	}

	public void addHeart()
	{
		if (heartArray.Count >= count)
			return;
		heartArray.Push (instHeart ());
	}

	public void removeHeart()
	{
		if (heartArray.Count == 0)
			return;
		GameObject obj = heartArray.Pop () as GameObject;
		Destroy (obj);
		if (heartArray.Count == 0) {
			GameManager.instance.GameOver ();
		}
	}

	// Update is called once per frame
	void Update () {
		
	}
}
