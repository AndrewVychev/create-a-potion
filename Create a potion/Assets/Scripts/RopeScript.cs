﻿using UnityEngine;
using System.Collections;

public class RopeScript : MonoBehaviour 
{
	public float leftAngle;
	public float rightAngle;
	public float pushVelocity = 100f;

	public float maxPushVelocity = 500f;
	public float minPushVelocity = 100f;

	public float maxRightAngle = 0.5f;
	public float minLeftAngle = -0.5f;

	public float minRightAngle = 0.3f;
	public float maxLeftAngle = -0.3f;

	public float minGravity = 1f;
	public float maxGravity = 4f;

	public Rigidbody2D body2d;

	void Awake()
	{
		body2d = GetComponent<Rigidbody2D> ();
		body2d.angularVelocity = pushVelocity;
	}

	public void AddGravity(float deltaGravity)
	{
		body2d.gravityScale = Mathf.Min (body2d.gravityScale + deltaGravity, maxGravity);
	}

	public void DecGravity(float deltaGravity)
	{
		body2d.gravityScale = Mathf.Max (body2d.gravityScale - deltaGravity, minGravity);
	}

	public void AddVelocity(float deltaVelocity)
	{
		pushVelocity = Mathf.Min (pushVelocity + deltaVelocity, maxPushVelocity);
	}

	public void DecVelocity(float deltaVelocity)
	{
		pushVelocity = Mathf.Max (pushVelocity - deltaVelocity, minPushVelocity);
	}

	public void DecAngle(float deltaAngle)
	{
		rightAngle = Mathf.Max (minRightAngle, rightAngle - deltaAngle);
		leftAngle = Mathf.Min (maxLeftAngle, leftAngle + deltaAngle);
	}

	public void AddAngle(float deltaAngle)
	{
		rightAngle = Mathf.Min (maxRightAngle, rightAngle + deltaAngle);
		leftAngle = Mathf.Max (minLeftAngle, leftAngle - deltaAngle);
	}

	void Update()
	{
		Push ();
	}

	public void Push()
	{
		if (transform.rotation.z > 0
		    && transform.rotation.z < rightAngle
		    && (body2d.angularVelocity > 0)
		    && body2d.angularVelocity < pushVelocity)
		{
				body2d.angularVelocity = pushVelocity;
		}
		else
		if (transform.rotation.z < 0
			&& transform.rotation.z > leftAngle
			&& (body2d.angularVelocity < 0)
				&& body2d.angularVelocity > pushVelocity * (-1))
		{
				body2d.angularVelocity = pushVelocity * (-1);
		}
	}
}
