﻿using UnityEngine;
using System.Collections;
using System;

public class TurningScript : MonoBehaviour {

	public float rotationSpeed = 10f;
	public float leftAngle = -45f;
	public float rightAngle = 45f;
	public float startAngle = 0f;
	public bool movingRight = true;

	private float movingDir = 1f;
	private float locRightAngle;
	private float locLeftAngle;

	void Start()
	{
		locLeftAngle = leftAngle + 180f;
		locRightAngle = rightAngle + 180f;
		if (movingRight)
			movingDir = 1f;
		else
			movingDir = -1f;
		transform.eulerAngles = new Vector3 (0f, 0f, startAngle);
	}

	void Update() 
	{
		if (transform.eulerAngles.z >= locRightAngle) 
		{
			movingDir = -1f;
			movingRight = false;
		} 
		else 
		if (transform.eulerAngles.z <= locLeftAngle) 
		{
			movingRight = true;
			movingDir = 1f;
		}
		transform.Rotate (new Vector3(0, 0, rotationSpeed * movingDir * Time.deltaTime));
	}
}
