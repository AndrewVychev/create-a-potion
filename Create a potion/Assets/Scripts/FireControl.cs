﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FireControl : MonoBehaviour {

	public Slider slider;
	public Transform fireTransform;
	public Animator canvasAnimator;
	public Animator cauldronAnimator;
	private Vector3 startScale;
	private SectorController sc;

	// Use this for initialization
	void Start () {
		startScale = fireTransform.localScale;
		sc = slider.GetComponent<SectorController> ();
		FireControlEvent ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void FireControlEvent()
	{
		float percent = slider.value / slider.maxValue;
		fireTransform.localScale = new Vector3(fireTransform.localScale.x, 
			startScale.y * percent, fireTransform.localScale.z);
		float multiplier = sc.GetMult ();
		if (multiplier < 1) {
			canvasAnimator.SetBool ("ThermoPulse", true);
			cauldronAnimator.SetBool ("IsBlink", true);
		} else {
			canvasAnimator.SetBool ("ThermoPulse", false);
			cauldronAnimator.SetBool ("IsBlink", false);
		}
	}

}
