﻿using UnityEngine;
using System.Collections;

public class PutScript : MonoBehaviour {

	public float pushPower = 5f;
	public float scorePoints = 10f;
	private float pushingVelocity;

	private Rigidbody2D m_rigidbody;
	// Use this for initialization
	void Start () 
	{
		m_rigidbody = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			m_rigidbody.isKinematic = false;
			RopeScript rs = GetComponentInParent<RopeScript> ();
			if (!rs)
				return;
			pushingVelocity = rs.body2d.angularVelocity;

			transform.SetParent (null);
			m_rigidbody.AddForce (transform.right * pushPower * pushingVelocity);
		}
	}
}
