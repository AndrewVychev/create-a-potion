﻿using UnityEngine;
using System.Collections;

public class MovingCauldron : MonoBehaviour {

	public float maxSpeed = 10f;
	public float speed = 2f;
	public float minSpeed = 2f;
	public int direction = 1;

	// Use this for initialization
	void Start () {
	
	}

	public void DecreaseSpeed(float deltaSpeed)
	{
		speed = Mathf.Max (speed - deltaSpeed, minSpeed); 
	}

	public void IncreaseSpeed(float deltaSpeed)
	{
		speed = Mathf.Min (speed + deltaSpeed, maxSpeed); 
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		WallDirection wd = collider.gameObject.GetComponent<WallDirection> ();
		if (wd)
			direction = wd.direction;
	}

	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (transform.position.x + direction * speed * Time.deltaTime, 
			transform.position.y, transform.position.z);
	}
}
